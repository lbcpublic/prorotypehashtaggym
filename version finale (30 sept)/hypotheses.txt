Hypothèses :
    1. On présume que pour s'inscrire à un Service, confirmer son inscription, créer une Service
       ou consulter les inscriptions à ses Services, les membres/professionnels doivent d'abord s'authentifier
	   
    2. On met à jour les répertoires automatiquement
	
    3. Le professionnel ne peut ajouter de Services pour la journée courante
	
    4. Le professionnel peut ajouter des Services jusqu'à un an d'avance
	
    5. Le Membre peut confirmer son inscription à une Séance de 1h à 5 min avant l'heure de de la Séance
	
    6. Le Centre de données contient trois répertoires séparés afin de simplifier les opérations :
        - Le Répertoire des services
        - Le Répertoire des abonnés
        - Le Répertoire des inscriptions
		
    7. On présume qu'un seul service peut être donné à la fois. Éventuellement, il faudra vérifier si différents
       types de services peuvent être donnés simultanément
	  
    