


Cas d'utilisation:
	1. Authentifier 
	2. Inscrire client 
	3. Inscription à Activité
	4. Confirmer inscription à Activité
	5. Créer Activité
	6. Consulter inscriptions à Activité
	7. Accéder aux installations
	8. Consulter répertoire des services
	
Acteurs :
	a. Agent
	b. Client
	c. Membre
	d. Professionnel
		
------------------------------------------------------------
------------------------------------------------------------

Cas D'utilisations

------------------------------------------------------------

Nom : Authentifier

But : L'Agent authentifie un Abonné pour lui donner accès aux autres fonctionnalités du Centre de données ou au Centre

Acteurs : Membre | Professionnel (principal), Agent (Secondaire)

Préconditions : L'abonné se présente à la réception et demande à l'Agent d'exécuter une opération requérant une authentification.

Scénario principal :
    1. L'Agent entre le numéro de l'abonné ainsi que le statut d'accès demandé.
    2. Le Centre de données vérifie que l'abonné possède un numéro valide, qu'il possède le statut d'accès demandé
       et que sa période d'inscription n'est pas échue, s'il y a lieu.
    3. Le numéro de membre est confirmé valide, le statut d'accès demandé est correct et l'inscription n'est pas échue.
    4. Le message "Validé" apparait à l'écran
    
Scénarios alternatifs :
    3a. Le numéro de membre n'existe pas
    4a. Le message "Numéro invalide" apparait à l'écran
    
    3b. Le statut d'accès demandé est invalide
    4b. Le message "Statut d'accès invalide" apparait à l'écran
    
    3c. L'inscription en tant que Membre est échue ou le Professionnel s'est désinscrit
    4c. Le message "Membre suspendu" apparait à l'écran
    
Post-conditions : 

------------------------------------------------------------

Nom : Consulter répertoire des services

But : L'Agent recherche un service ou des inscriptions à un service à la demande d'un abonné

Acteurs : Membre | Professionnel (principal), Agent (Secondaire)

Préconditions : L'abonné s'est présenté à la réception et a demandé à l'Agent d'exécuter une opération requérant une consultation du répertoire des services

Scénario principal :
    1. L'Agent choisit entre une recherche par professionnel, une recherche par numéro, par texte ou par heure, ou d'afficher tous les services.
    2. L'Agent entre le numéro ou le texte à chercher	
    3. Le Centre de données affiche le ou les services correspondant à la requête
    4. L'Agent sélectionne le service de son choix

Scénarios alternatifs :
    3a. La recherche ne donne aucun résultat. Le Centre de données demande à l'Agent s'il veut effectuer une nouvelle recherche ou abandonner.
    4a. Si l'Agent veut effectuer une nouvelle recherche, retour à l'étape 1
    
Post-conditions : Aucune

------------------------------------------------------------

Cas d'utilisation: Inscrire client

But: 	L'Agent enregistre l'information personnelles d'un Client dans le Centre de Données pour
		créer un nouvel abonné.

Préconditions: Le Client doit ne pas déjà être abonné.

Acteurs: Client (principal), Agent (secondaire)

Scénario principal: 

	1. Le Client demande à l'Agent de s'abonner au GYM
	2. L'Agent entre les informations personnelles du Client
		2.1 L'Agent demande de l'information au Client
		2.2 L'Agent entre l'information dans son ordinateur
	3. Toute l'information est envoyé au Centre de Données où elle est enregistrée.
	
	
Scénarios alternatifs:

	2.a.1 Le système informe que Le Client est déjà abonné
	2.a.2 L'Agent lui informe qu'il est déjà inscrit
		
	
Postconditions
	Un nouvel abonné est crée dans le Centre de Données.
	
------------------------------------------------------------

Nom: Inscrire à activité

But: 	Inscrire un Membre à une activité du GYM.  

Préconditions: La personne doit être un Membre du GYM.

Acteurs: Membre (principal), Agent (secondaire)

Scénario principal: 

	1. L'Agent authentifie le Membre (CU: authentifier).
	2. L'Agent consulte répertoire des services (CU: consulter répertoire des services)
	3. Le Membre choisit une activité
	4. L'Agent enregistre l'information suivante dans le Centre de Données:
		Date et heure actuelles
		Date à laquelle du service qui sera fourni
		Numéro du professionnel
		Numéro du Membre
		Code du service
		Commentaires (facultatif).
	
Scénarios alternatifs:

	1.a. L'Agent est incapable d'authentifier le Membre
		1.a.1. L'Agent informe la personne que ce numéro de Membre n'existe pas. Il lui donne la raison de l'erreur.
			1.a.2.1. S'il n'est pas membre, l'Agent lui propose de s'inscrire comme membre. (CU Ajouter client)
			1.a.2.2. Sinon, il lui refuse l'inscription.
	
	
Postconditions

	L'inscription du Membre à l'Activité est enregistrée dans le Centre de Données.
	
	
------------------------------------------------------------

Nom: Confirmer inscription à une Activité

But: 	Le Membre confirme son inscription à une activité en se présentant à la réception du GYM. L'Agent enregistre la confirmation de sa présence. 
		Ceci confirme que le service a été fourni.

Préconditions: 
	- La personne doit être Membre du GYM.
	- Elle doit aussi être inscrite à l'Activité
	- Le Membre se présente à la réception dans une période conforme à l'hypothèse 5

Acteurs: Membre (principal), Agent (secondaire)

Scénario principal: 

	1. Authentifier le Membre. (CU authentifier)
	2. Le Membre fournie le code du service de l'activité
	3. L'Agent consulte répertoire des services (CU: consulter répertoire des services)
	4. L'Agent vérifie que le Membre est incrit à l'Activité
	5. L'Agent envoie la confirmation l'information suivante au Centre de Données, où elle est enregistrée.
	
		Date et heure actuelles
		Numéro du professionnel
		Numéro du membre
		Code du service
		Commentaires (facultatif).
	
	
Scénarios alternatifs:

	1.a. L'Agent est incapable d'authentifier le Membre
		1.a.1. L'Agent informe la personne que ce numéro de Membre n'existe pas. Il lui donne la raison de l'erreur.
			1.a.2.1. S'il n'est pas membre, l'Agent lui propose de s'inscrire comme membre. (CU Ajouter client)
			1.a.2.2. Sinon, il lui refuse la confirmation.
			
	3.a L'activité n'existe pas
		3.a.1. L'Agent informe le membre que l'activité n'existe pas.
		
		
	
	4.a. Le membre n'est pas inscrit à l'activité
		4.a.1. L'Agent informe le membre qu'il n'est pas inscrit à l'activité. Il lui propose de s'y inscrire, s'il y a de la place.(CU : Inscrire à activité)
		4.a.2. S'il n'y a pas de place, il lui dit que c'est complet. 


Postconditions
	La confirmation que le service a été fourni est enregistrée dans le Centre de Données.
	
------------------------------------------------------------
   
Nom: Créer activité
    But: Le Professionnel demande à l'Agent de créer une nouvelle activité au GYM. 
	
    Acteurs: Professionnel (principal), Agent de réception (secondaire)
	
    Préconditions: Être Professionnel, Activité doit être disponible au membre le jour suivant 
    ou plus tard. 
	
    Scénario principal: 
	    1. Authentification (CU: authentifier)
		2. Consulter répertoire des services (CU: Consulter répertoire des services)
	    3. Créer l'activité
			3.1 L'Agent demande l'information suivante au Professionnel
				Date de début du service
				Date de fin du service
				Heure du service
				Récurrence hebdomadaire du service (quels jours il est offert à la même heure)
				Capacité maximale
				Numéro du professionnel
				
			3.2 Il rajoute l'information suivante:
				Code du service
				Commentaires (facultatif).
		4. L'Agent vérifie qu'il n'y a pas de conflits horaires.
				
		5. L'Agent enregistre l'information dans le Centre de Données
		
	    
    Scénario alternatifs:
			
		1.a. L'Agent est incapable d'authentifier le Professionnel
			1.a.1. L'Agent informe la personne que ce numéro de Professionnel est invalide.
			Il lui propose de s'inscrire comme professionnel. (CU: Ajouter client)
			
		4.a. Il y a un conflit horaire
			4.a.1. L'agent propose au Professionnel de choisir un autre horaire.
			 
	    
    Postconditions: 
		Le répertoire est mis à jour et l'activité est enregistrée et disponible pour
		être consultée par les membres. 
    
------------------------------------------------------------
	
Nom: Consulter inscriptions à activité

    But:	Le Professionnel demande à l'Agent de consulter la liste 
			des Membres inscrits à ses séances. L'Agent de Réception fait la requête au 
			centre de données et retourne l'information demandée au Professionnel.
	
    Acteurs: Professionnel (principal), Agent de réception (secondaire)
	
	Préconditions: Être Professionnel et avoir créé une Activité
	
    Scénario principal: 
		
		1. Authentification (CU: authentifier)
		
		2  Le professionnel donne à l'Agent le code de service qu'il veut consulter.
		
		3. L'agent consulte le répertoire des services (CU: Consulter répertoire des services)
		
		4. L'Agent sélectionne le service correspondant pour afficher ses détails.
			4.1. L'Agent montre la liste des inscrits au Professionnel.
	
	    
    Scénario alternatifs:
	
		1.a. L'Agent est incapable d'authentifier le Professionnel
			1.a.1. L'Agent refuse de donner l'information au Professionnel.
			
		4.a L'activité n'existe pas
			4.a.1. L'Agent informe le membre que l'activité n'existe pas.
	
    Postconditions:
		Le professionnel a maintenant la liste des membres inscrits à ses séances.
    
------------------------------------------------------------

Nom: Accéder aux installations

    But: Un membre se présente à l'agent pour accéder aux locaux. 
	
	Acteurs: Membre (principal), Agent de réception (secondaire)
	
    Préconditions: Être membre
    
    Scénario principal:
	    1. Authentification (CU: authentifier)
		2. L'Agent authorise l'accès au Membre.

    Scénario alternatifs:
	
		1.a. L'Agent est incapable d'authentifier le Membre
			1.a.1. L'Agent informe la personne que ce numéro de Membre n'existe pas. Il lui donne la raison de l'erreur.
				1.a.2.1. S'il n'est pas membre, l'Agent lui propose de s'inscrire comme membre. (CU Ajouter client)
				1.a.2.2. Sinon, il lui refuse l'accès.
	
