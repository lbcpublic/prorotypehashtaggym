
INCOMPLET

------------------------------------------------------------

Cas d'utilisation: Lire des données du CD
But: Exécuter une requête qui extrait du CD de l'information pertinente .
Préconditions: La personne accédant est un/une réceptioniste
Acteurs: Réceptioniste (principal)
Scénario principal: 

	1. Le réceptioniste exécute une requête qui est envoyé au CD
	2. Le réceptioniste reçoit une réponse contenant l'information
	3. Le réceptioniste ferme la session
	
Scénario alternatifs:
	
	1a.1 Le serveur ne répond pas
	1a.2 Le réceptioniste passe à l'étape 3
	
Postconditions	

------------------------------------------------------------

Cas d'utilisation: Écrire des données dans le CD
But: Exécuter une requête qui écrit de ajoute/modifie de l'information dans le CD.
Préconditions: La personne accédant est un/une réceptioniste
Acteurs: Réceptioniste (principal)
Scénario principal: 

	1. Le réceptioniste exécute une requête qui est envoyé au CD
	2. Le réceptioniste reçoit une réponse de confirmation
	3. Le réceptioniste ferme la session
	
Scénario alternatifs:
	
	1a.1 Le serveur ne répond pas
	1a.2 Le réceptioniste passe à l'étape 3
	
Postconditions

------------------------------------------------------------

Cas d'utilisation: Obtenir accès au #GYM
But: Ouvrir le tourniquet pour la personne en question
Préconditions: La personne doit être déja abbonné au GYM
Acteurs: 
	Personne (principal)
	Réceptioniste (secondaire)
	
Scénario principal: 
	
	1. La personne ne présente à la réception
	2. La personne fournit un pièce d'identité au réceptioniste (flou)
	3. Le réceptioniste utilise le CU "Lire du CD" pour rechercher le nom de la personne.
	4. Le numéro retourné est valide.
	5. Le réceptioniste ouvre le tourniquet à la personne.
	
Scénario alternatifs:
	
	1a.1 Le serveur ne répond pas
	1a.2 Le réceptioniste passe à l'étape 3
	
	2a.1 La personne n'a pas de pièce d'identité sur elle.
	2a.2 La personne rentre chez elle chercher sa pièce d'identité. (possible de montrer photo?)
	2.a.3 Passer à l'étape 3
	
	3.a. Même scénario erreur de "Lire des données du CD"
	
	
Postconditions

------------------------------------------------------------

Cas d'utilisation: S'abonner au gym
But: Enregistrer une personne dans le CD et lui assigner un code pour qu'elle puisse accéder aux facilités de #GYM dorénavant.
Préconditions: La personne doit ne pas déjà être membre.
Acteurs: 
	Personne (principal)
	Réceptioniste (secondaire)
	
Scénario principal: 
	
	1. La personne ne présente à la réception
	2. La personne fournit au réceptioniste ses informations personnelles
	3. Le réceptioniste utilise le CU "Écrire au CD" pour enregistrer l'information reçue.
	4. Le CD retourne un numéro unique.
	
Scénario alternatifs:
	
	3.a. Même scénario erreur de "Écrire des données au CD"
	
	3.b.1. Le CD retourne l'exception: "Personne déjà membre" (flou: 2 personnes mêmes information personnelle)
	
	3.b.2. Le réceptioniste dit à la personne qu'elle est déja membre.
	
	
Postconditions





------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------

Extras

1. reception to subscribe to #GYM

2. reception to access facilities

3. reception to register to classes

4. reception to get listed as trainer

5. reception to create classes

6. reception to confirm presence

7. 


use cases:

GYM receptionist 

	Write data in centre de données
	Read data from centre de données
	Subscribe to #GYM
	Enter #GYM
	Register to class
	Confirm Presence to class
	Register as professional
	Create class
	Check class attendance
	
Normal person		Professional
	

	
	