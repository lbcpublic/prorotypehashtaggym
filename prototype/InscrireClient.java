import java.util.Scanner;

public class InscrireClient extends Menu {

    public int run(){
        Scanner scanner = new Scanner(System.in);
        String input;

        System.out.println("** Pour le cas de client déja inscrit, entrer 1, puis 1. **\n");

        System.out.print("Nom du client : ");
        input = scanner.nextLine();

        System.out.print("Addresse : ");
        if(input.compareTo("1") == 0){
            if(input.compareTo(scanner.nextLine()) == 0){
                System.out.println("Ce client est déja inscrit (numéro : 1234566).");
            }
        }else{
            scanner.nextLine();
            System.out.println("Le nouveau client a été inscrit.");
        }
        return 0;
    }
}

