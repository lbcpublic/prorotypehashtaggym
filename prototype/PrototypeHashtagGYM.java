import java.util.Scanner;

public class PrototypeHashtagGYM {
    public static void main(String[] args){
        ConsulterRepertoireServices rs = new ConsulterRepertoireServices();
        Menus etatMenu = Menus.Start;
        StartMenu start = new StartMenu();
        int resultat;
        Scanner scanner = new Scanner(System.in);
        while(true){
            switch(etatMenu){
                case Start: {
                    resultat = start.run();
                    switch (resultat) {
                        case 0:
                            etatMenu = Menus.AccederInstallations;
                            break;
                        case 1:
                            etatMenu = Menus.InscrireClient;
                            break;
                        case 2:
                            etatMenu = Menus.InscrireMembreActivite;
                            break;
                        case 3:
                            etatMenu = Menus.ConfirmerInscriptionActivite;
                            break;
                        case 4:
                            etatMenu = Menus.CreerActivite;
                            break;
                        case 5:
                            etatMenu = Menus.ConsulterInscriptionsActivite;
                            break;
                        case 6:
                            etatMenu = Menus.Quitter;
                            break;
                        default: {
                            throw new IllegalStateException("Atteinte d'un état illégal dans le menu Start.");
                        }
                    }
                    break;
                }
                case AccederInstallations:{
                    MenuAuthentifier auth = new MenuAuthentifier();
                    auth.run();
                    System.out.print("Appuyez sur Entrée pour retourner au menu principal.");
                    scanner.nextLine();
                    etatMenu = Menus.Start;
                    break;
                }
                case InscrireClient:{
                    InscrireClient inscr = new InscrireClient();
                    inscr.run();
                    System.out.print("Appuyez sur Entrée pour retourner au menu principal.");
                    scanner.nextLine();
                    etatMenu = Menus.Start;
                    break;
                }
                case InscrireMembreActivite:{
                    MenuAuthentifier auth = new MenuAuthentifier();
                    int nAbonne = auth.run();
                    if(nAbonne == 1234566){
                        while(true){
                            rs.run();
                            InscrireMembreActivite inscr = new InscrireMembreActivite();
                            if(inscr.run() != 0
                                    || scanner.nextLine().compareToIgnoreCase("n") == 0){
                                break;
                            }
                                
                            
                        }
                        rs.inscrireMembre(2342352);                 
                    }
                    System.out.print("Appuyez sur Entrée pour retourner au menu principal.");
                    scanner.nextLine();
                    etatMenu = Menus.Start;
                    break;
                }
                case ConfirmerInscriptionActivite:{
                    MenuAuthentifier auth = new MenuAuthentifier();
                    int nAbonne = auth.run();
                    if(nAbonne == 1234566){
                        rs.run();
                        ConfirmerInscriptionActivite confirmation = new ConfirmerInscriptionActivite(nAbonne);
                        confirmation.run();
                    }
                    System.out.print("Appuyez sur Entrée pour retourner au menu principal.");
                    scanner.nextLine();
                    etatMenu = Menus.Start;
                    break;
                }
                case CreerActivite:{
                    MenuAuthentifier authCA = new MenuAuthentifier();
                    int noProf = authCA.run();
                    if (noProf==1234567){
                        rs.run();
                        CreerActivite act = new CreerActivite(noProf,2342354);
                        act.run();
                        rs.creerActivite(act.getInfo(),act.getInscriptions());
                        rs.run();
                    }
                    System.out.print("Appuyez sur Entrée pour retourner au menu principal.");
                    scanner.nextLine();
                    etatMenu = Menus.Start;
                    break;
                }
                case ConsulterInscriptionsActivite:{
                    MenuAuthentifier authCIA = new MenuAuthentifier();
                    int noProf = authCIA.run();
                    if (noProf==1234567){
                        rs.run();
                        ConsulterInscriptionsActivite cIA = new ConsulterInscriptionsActivite();
                        cIA.run();
                        rs.consulterInscriptions(cIA.getCodeService());
                    }
                    System.out.print("Appuyez sur Entrée pour retourner au menu principal.");
                    scanner.nextLine();
                    etatMenu = Menus.Start;
                    break;
                }
                case Quitter:{
                    System.out.println("Merci et à la prochaine!\n");
                    return;
                }
                default:{
                    throw new IllegalStateException("Atteinte d'un état illégal dans les menus.");
                }
            }
        }
    }
}
