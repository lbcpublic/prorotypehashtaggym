import java.util.Scanner;

public class ConfirmerInscriptionActivite extends Menu{
    int nAbonne;

    public ConfirmerInscriptionActivite(int nAbonne){
        this.nAbonne = nAbonne;
        this.text = new String[]{
                "Le client n'est inscrit qu'à l'activité 2342352, pas l'activité 2342353.\n\n" +
                "Veuillez entrer le code de l'activité : "

        };
    }

    public int run() {
        Scanner scanner = new Scanner(System.in);

        int codeActivite;
        String input;
        //Boucle while et
        //mécanisme try-catch pour re-essayer si il y a erreur.
        while (true) {
            System.out.print(this.text[0]);
            input = scanner.nextLine();
            try {
                codeActivite = Integer.parseInt(input);
                break;
            } catch (Exception e) {
                System.out.print("\"" + input + "\" n'est pas un nombre de format valide.\n" +
                        "Souhaitez-vous :\n 1. Recommencer\n" +
                        " 2. Abandonner ?\n" +
                        "Choix (1 par défaut): ");
                if (scanner.nextLine().compareToIgnoreCase("2") == 0) {
                    return -1;
                }
            }
        }

        if (this.nAbonne == 1234566) {
            if(codeActivite == 2342352){
                System.out.println("Inscription confirmée.");
                System.out.println("Numéro professionnel: 1234567");
                System.out.println("Numéro du membre: 1234566");
                System.out.println("Code du service: 2342352");
                    
            }else if(codeActivite == 2342353){
                System.out.println("Le client n'est pas inscrit à cette activité.");
            }else{
                System.out.println("L'activité n'existe pas.");
            }
        }
        else{
            System.out.println("Ce client n'est inscrit à aucune activité");
        }

        return -1;
    }

}
