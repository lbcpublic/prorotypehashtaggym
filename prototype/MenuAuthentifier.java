import java.util.Scanner;

public class MenuAuthentifier extends Menu {
    public MenuAuthentifier(){
        this.text = new String[]{
            "Veuillez entrer le numéro de l'abonné : ",
            "Veuillez entrer le statut d'accès demandé par l'abonné :\n" +
            " 1. Membre\n" +
            " 2. Professionnel\n"
        };
    }

    @Override
    public int run() {
        Scanner scanner = new Scanner(System.in);
        int number;
        int access;
        String input;
        while (true){
            System.out.print(this.text[0]);
            input = scanner.nextLine();
            try{
                number = Integer.parseInt(input);
                break;
            }catch (Exception e){
                System.out.print("\"" + input + "\" n'est pas un nombre de format valide.\n" +
                                   "Souhaitez-vous :\n 1. Recommencer\n" +
                                   " 2. Abandonner l'authentification?\n" +
                                   "Choix (1 par défaut): ");
                if(scanner.nextLine().compareToIgnoreCase("2") == 0){
                    return -1;
                }
            }
        }

        while (true){
            System.out.print(this.text[1]);
            input = scanner.nextLine();
            try{
                access = Integer.parseInt(input);
                if(access != 1 && access != 2){
                    System.out.print("Option invalide.\n");
                    continue;
                }
                break;
            }catch (Exception e){
                System.out.print("\"" + input + "\" n'est pas un nombre valide.\n" +
                        "Souhaitez-vous :\n 1. Recommencer\n" +
                        " 2. Abandonner l'authentification?\n" +
                        "Choix (1 par défaut): ");
                if(scanner.nextLine().compareToIgnoreCase("2") == 0){
                    return -1;
                }
            }
        }

        if(number == 1234566){
            if(access == 1){
                System.out.println("Validé\n");
                return number;
            }else{
                System.out.println("Statut d'accès invalide.\n");
                return 1;
            }
        }else if(number == 1234567){
            if(access == 2){
                System.out.println("Validé\n");
                return number;
            }else{
                System.out.println("Statut d'accès invalide.\n");
                return 1;
            }
        }else if(number == 1234568){
            System.out.println("Membre suspendu\n");
            return 0;
        }else{
            System.out.println("Numéro invalide\n");
            return -1;
        }
    }
}
