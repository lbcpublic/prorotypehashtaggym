import java.util.Scanner;

public class StartMenu extends Menu {

    public StartMenu(){
        this.text = new String[]{"\n\n" +
                                 "------------------------------------------------------------\n" +
                                 "|                      DIRECTIVES:                         |\n" +
                                 "------------------------------------------------------------\n" +
                                 "    Ce logigiel est un prototype, et donc n'est pas encore\n" +
                                 "    entièrement fonctionnel.\n" +
                                 "    Il existe l'option d'inscrire de nouveaux clients\n" +
                                 "    et de créer nouvelles séances, mais dans ce prototype,\n" +
                                 "    leurs informations ne sont pas stockées.\n" +
                                 "    Il est donc seulement possible d'authentifier et de\n" +
                                 "    rechercher les données mentionnées ci-dessous.\n" +
                                 "    Plus d'infos dans le rapport. \n\n" +
                                 "    Membres:  1234566, 1234568 (suspendu)\n" +
                                 "    Professionnels: 1234567\n" +
                                 "    Activités: 2342352, 2342353" +
                                 "\n\n" +
                                 "------------------------------------------------------------\n" +
                                 "|            Bienvenue au Centre de données #GYM.           |\n" +
                                 "|        Veuillez choisir une des options suivantes :       |\n" +
                                 "------------------------------------------------------------\n" +
                                 "    0  Autoriser l'accès à aux installations\n" +
                                 "    1  Inscrire un client à #GYM\n" +
                                 "    2  Inscrire un membre à une activité\n" +
                                 "    3  Confirmer l'inscription d'un membre à une activité\n" +
                                 "    4  Créer une activité\n" +
                                 "    5  Consulter les inscriptions à une activité\n" +
                                 "    6  Quitter\n\n" +
                                 "    Choix : "};
    }

    public int run(){
        Scanner scanner = new Scanner(System.in);
        String reply;

        while (true){
            System.out.print(this.text[0]);
            reply = scanner.nextLine();

            try{
                return Integer.parseInt(reply);
            }catch(Exception e){
                System.out.println("\"" + reply + "\" n'est pas une réponse valide. " +
                        "Souhaitez-vous recommencer (O/n)? ");
                reply = scanner.nextLine();
                if(reply.compareToIgnoreCase("n") == 0){
                    return 6;
                }
            }
        }
    }
}
