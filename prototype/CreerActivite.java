import java.util.Scanner;

public class CreerActivite extends Menu {
    int [] inscriptions;
    int indexInscription = 0;
    int [] repQuestions = new int[7];
    String commentaire;
    String texte = " ________________Créer une Activité________________ \n" +                 "Prototype: Le code sera: 2342354\n" + 
                   "Veuillez répondre aux questions suivantes: \n";
    Scanner scanner = new Scanner(System.in);
    String [] questionnaire = new String[]{"","",
                                           "Date de début (1 à 7): \n",
                                           "Date de fin (entier): \n",
                                           "Heure (1 à 24): \n",
                                           "Jour de réccurence (1 à 7): \n",
                                           "Capacité maximale (entier): \n"};
    
    public CreerActivite(int noProf, int codeService){
        this.repQuestions[0] = codeService;
        this.repQuestions[1] = noProf;
    }

    @Override
    public int run(){
        System.out.print(this.texte);
        for (int i=2; i<this.questionnaire.length; i++){
            System.out.print(questionnaire[i]);
            String reponse = scanner.nextLine();
            while (true){
                try{
                    this.repQuestions[i] = Integer.parseInt(reponse);
                    break;
                } catch (Exception e){
                    System.out.print("Essayer encore: ");
                    reponse = scanner.nextLine();
                }
            }
        }
        System.out.print("Commentaires: \n");
        this.commentaire = scanner.nextLine();
        this.inscriptions = new int[this.repQuestions[6]];
        return 0;

    }

    public int[] getInfo(){
        return this.repQuestions;
    }

    public int[] getInscriptions(){
        return this.inscriptions;
    }

    public void setInscription(int noMembre){
        if (this.indexInscription >= this.inscriptions.length){
            this.inscriptions[indexInscription] = noMembre;
            this.indexInscription++;
        } else {
            System.out.print("Inscriptions complètes pour cette activité!");
        }
    }

    

    

    
}
