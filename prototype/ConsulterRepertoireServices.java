import java.util.HashMap;


public class ConsulterRepertoireServices extends Menu {
    private String[] texte;
    private int compteur;
    private HashMap<Integer, int[]> activites = new HashMap<Integer, int[]>();
    private HashMap<Integer, int[]> infoService = new HashMap<Integer, int[]>();
    private Boolean [][] horaire = new Boolean[24][7];
    private int [][] horaireActivite = new int [24][7];

    public ConsulterRepertoireServices(){
        this.compteur = 0;
        this.texte = new String[]{" Dim      ",
                                  " Lun      ",
                                  " Mar      ",
                                  " Mer      ",
                                  " Jeu      ",
                                  " Ven      ",
                                  " Sam      "};
        
        for (int i=0; i<this.horaire.length;i++){
            for (int j=0; j<this.horaire[0].length;j++){
                this.horaire[i][j]=true;
            }
        }
    }


    public void afficher(Boolean[][] tableau){
        System.out.print("\n");
        int heure = -1;
        for (int i=heure; i<tableau.length; i++){
            if (heure < 10){
                System.out.print(heure++ +" ");
            } else {
                System.out.print(heure++);
            }
            
            for (int j=0; j<tableau[0].length; j++){
                if (i==-1){
                    System.out.print(this.texte[j]);
                } else {
                    if (tableau[i][j]==true){
                        System.out.print(" Libre    ");
                    } else {
                        System.out.print(" "+horaireActivite[i][j]+"  ");
                    }
                }                
            }
            
            System.out.print("\n");
        }
        
        System.out.print("\n");
    }
    
    @Override
    public int run() {
        if (this.compteur == 0){
            this.hardCode();
        }
        this.afficher(this.horaire);
        this.afficherServices();
        this.compteur++;
        return 0;
    }

    public void creerActivite(int[] activite, int[] inscriptions){
        int jour = activite[2]-1;
        int heure = activite[4];
        int codeService = activite[0];
        if (this.horaire[heure][jour]==true){
            this.horaire[heure][jour] = false;
            this.horaireActivite[heure][jour] = codeService;
        } else {
            System.out.println("Conflit d'horaire!");
            return;
        }
        this.activites.put(activite[0],inscriptions);
        this.infoService.put(activite[0],activite);
    }

    public void consulterInscriptions(int  codeService){
        if (this.activites.containsKey(codeService)){       
            int [] inscrits = this.activites.get(codeService);
            if (inscrits[0]==0){
                System.out.print(" Il n'y a  pas d'inscrits! \n");
            } else {
                System.out.print("Liste des inscriptions: \n");
                for (int i=0; i<inscrits.length; i++){
                    System.out.println(inscrits[i]);
                }
            }
            
        } else {
            System.out.print(" Cette activité n'existe  pas! \n");
        }
    }

    public void hardCode(){
        int[] a = new int[]{2342353,1234567,6,23,11,6,2};
        int[] b = new int[]{8888888,9999999};

        int[] c = new int[]{2342352,1234567,3,23,9,3,6};
        int[] d = new int[]{7777777};
        this.creerActivite(a,b);
        this.creerActivite(c,d);
    }

    public void afficherServices(){
        System.out.println("CodeService NomService HeureDebut HeureFin"); 
        for (int i: this.infoService.keySet()){
            if (this.infoService.get(i)[4] < 10){
                System.out.println(i+"    " +" Entraineur "+ this.infoService.get(i)[4] + "          " + (this.infoService.get(i)[4]+1));
            } else {
                System.out.println(i+"    " +" Entraineur "+ this.infoService.get(i)[4] + "         " + (this.infoService.get(i)[4]+1));
            }
            
        }

        System.out.print("\n");
    }

    public void inscrireMembre(int noService){
        int[] actuel= this.activites.get(noService);
        int longueur = this.infoService.get(noService)[6];
        int[] rajout = new int[longueur];
        for (int i=0; i<actuel.length; i++){
            rajout[i] = actuel[i];
        }
        rajout[1] = 1234566;
        this.activites.put(noService,rajout);
        
    }
    
        
}

