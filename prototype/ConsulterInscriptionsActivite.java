import java.util.Scanner;

public class ConsulterInscriptionsActivite extends Menu {

    private int codeService;
      
    public ConsulterInscriptionsActivite(){
        this.text = new String[]{" ______________Consulter les Inscriptions______________ \n",
                                 " Veuillez entrer le code d'activité :"};
    }

    @Override
    public int run(){
        System.out.print(this.text[0]);
        String input;
        Scanner scanner = new Scanner(System.in);
        int number;
        while (true){
            System.out.print(this.text[1]);
            input = scanner.nextLine();
            try{
                number = Integer.parseInt(input);
                this.codeService = number;
                return 0;
            }catch (Exception e){
                System.out.print("\"" + input + "\" n'est pas un code de format valide.\n" +
                        "Souhaitez-vous :\n 1. Recommencer\n" +
                        " 2. Abandonner la consultation?\n" +
                        "Choix (1 par défaut): ");
                if(scanner.nextLine().compareToIgnoreCase("2") == 0){
                    return -1;
                }
            }
        }
    }

    public int getCodeService(){
        return this.codeService;
    }
    

}
