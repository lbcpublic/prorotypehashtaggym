
import java.util.Scanner;

public class InscrireMembreActivite extends Menu{

    public int run() {
        Scanner scanner = new Scanner(System.in);
        String input;
        int number;

        while (true){
            System.out.println("L'activité ayant encore des places a pour code 2342352");
            System.out.println("L'activité complète a pour code 2342353");
            System.out.print("Veuillez entrer le code d'activité  : ");
            input = scanner.nextLine();
            try{
                number = Integer.parseInt(input);
                break;
            }catch (Exception e){
                System.out.print("\"" + input + "\" n'est pas un nombre de format valide.\n" +
                        "Souhaitez-vous :\n 1. Recommencer\n" +
                        " 2. Abandonner l'authentification?\n" +
                        "Choix (1 par défaut): ");
                if(scanner.nextLine().compareToIgnoreCase("2") == 0){
                    return -1;
                }
            }
        }

        if(number == 2342352){
            System.out.println("Inscription confirmée.");
        }else if (number == 2342353){
            System.out.println("Cette activité est pleine.\n" +
                    "Souhaitez-vous sélectionner une autre activité (o/n)?");
            return 0;
        }

        return -1;
    }

}