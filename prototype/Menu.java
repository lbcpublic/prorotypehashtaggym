public abstract class Menu {
    protected String[] text; // Inventaire des textes à afficher lors des différentes étapes d'un menu

    public abstract int run();
}